package ru.bullyboo.shoe.shop.data;

/**
 * Created by BullyBoo on 14.07.2016.
 */
public class ReceivedData {

    public String id;
    public String name;
    public String country;
    public String region;
    public String town;
    public String address;
    public String metro;
    public String phone;
    public String worktime;
    public String type;
    public double longtitude;
    public double latitude;
    public String is_new;

    public double distance;

    public double x;
    public double y;

    public ReceivedData(String id,
                        String name,
                        String country,
                        String region,
                        String town,
                        String address,
                        String metro,
                        String phone,
                        String worktime,
                        String type,
                        String is_new,
                        double longtitude,
                        double latitude){
        this.id = id;
        this.name = name;
        this.country = country;
        this.region = region;
        this.town = town;
        this.address = address;
        this.metro = metro;
        this.phone = phone;
        this.worktime = worktime;
        this.type = type;
        this.is_new = is_new;
        this.longtitude = longtitude;
        this.latitude = latitude;
    }

    public void setId(String id) {
        this.id = id;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setCountry(String country) {
        this.country = country;
    }

    public void setRegion(String region) {
        this.region = region;
    }

    public void setTown(String town) {
        this.town = town;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public void setMetro(String metro) {
        this.metro = metro;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public void setWorktime(String worktime) {
        this.worktime = worktime;
    }

    public void setType(String type) {
        this.type = type;
    }

    public void setLongtitude(double longtitude) {
        this.longtitude = longtitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public void setIs_new(String is_new) {
        this.is_new = is_new;
    }

    public void setDistance(double distance) {
        this.distance = distance;
    }

    public void setX(double x) {
        this.x = x;
    }

    public void setY(double y) {
        this.y = y;
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getCountry() {
        return country;
    }

    public String getRegion() {
        return region;
    }

    public String getTown() {
        return town;
    }

    public String getAddress() {
        return address;
    }

    public String getMetro() {
        return metro;
    }

    public String getPhone() {
        return phone;
    }

    public String getWorktime() {
        return worktime;
    }

    public String getType() {
        return type;
    }

    public double getLongtitude() {
        return longtitude;
    }

    public double getLatitude() {
        return latitude;
    }

    public String getIs_new() {
        return is_new;
    }

    public double getDistance() {
        return distance;
    }

    public double getX() {
        return x;
    }

    public double getY() {
        return y;
    }
}

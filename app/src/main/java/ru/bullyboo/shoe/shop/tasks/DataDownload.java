package ru.bullyboo.shoe.shop.tasks;

import android.content.Context;
import android.os.AsyncTask;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.ArrayList;
import java.util.List;

import ru.bullyboo.shoe.shop.data.ReceivedData;
import ru.bullyboo.shoe.shop.interfaces.LoaderStatus;
import ru.bullyboo.shoe.shop.tools.CheckInternetConnection;


/**
 * Created by BullyBoo on 14.07.2016.
 */
public class DataDownload extends AsyncTask<Void, Void, String> {

    private LoaderStatus status;
    private Context context;

    private HttpURLConnection urlConnection;
    private BufferedReader readString;
    private String resultJsonParse = "";

    private List<ReceivedData> data;

    public DataDownload(Context context, LoaderStatus status){
        this.context = context;
        this.status = status;

        data = new ArrayList<>();
    }

    @Override
    protected String doInBackground(Void[] params) {
        try {

            CheckInternetConnection conection = new CheckInternetConnection(context);
            if(conection.isOnline()){
                //                получаем данные с ресурса
                URL yandexUrl = new URL
                        ("http://app.ecco-shoes.ru/shops/list");

                urlConnection = (HttpURLConnection) yandexUrl.openConnection();
                urlConnection.setRequestMethod("GET");
                urlConnection.connect();

                InputStream input = urlConnection.getInputStream();
                StringBuffer buffer = new StringBuffer();

                readString = new BufferedReader(new InputStreamReader(input));

                String s;

                while ((s = readString.readLine()) != null) {
                    buffer.append(s);
                }
                resultJsonParse = buffer.toString();
            }else{
                this.cancel(true);
                status.noInternetConnection();
            }

        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return resultJsonParse;
    }

    @Override
    protected void onPostExecute(String s) {
        super.onPostExecute(s);

        try {

            JSONObject json = new JSONObject(s);

            JSONArray jsonAr = json.getJSONArray("data");

            for (int i = 0; i < jsonAr.length(); i++) {

                String id = null;
                String name = null;
                String country = null;
                String region = null;
                String town = null;
                String address = null;
                String metro = null;
                String phone = null;
                String worktime = null;
                String type = null;
                double longtitude = 0;
                double latitude = 0;
                String is_new = null;

//                получаем данные о объекте
                JSONObject newArtist = jsonAr.getJSONObject(i);

//              проверяем наличие полей у выбранного объекта и записываем в массивы
                if (newArtist.has("id")) id = newArtist.getString("id");
                if (newArtist.has("name")) name = newArtist.getString("name");
                if (newArtist.has("country")) country = newArtist.getString("country");
                if (newArtist.has("region")) region = newArtist.getString("region");
                if (newArtist.has("town")) town = newArtist.getString("town");
                if (newArtist.has("address")) address = newArtist.getString("address");
                if (newArtist.has("metro")) metro = newArtist.getString("metro");
                if (newArtist.has("phone")) phone = newArtist.getString("phone");
                if (newArtist.has("worktime"))
                    worktime = newArtist.getString("worktime");
                if (newArtist.has("type")) type = newArtist.getString("type");
                if (newArtist.has("longtitude"))
                    longtitude = newArtist.getDouble("longtitude");
                if (newArtist.has("latitude"))
                    latitude = newArtist.getDouble("latitude");
                if (newArtist.has("is_new")) is_new = newArtist.getString("is_new");

                data.add(new ReceivedData(id, name, country,
                        region, town, address,
                        metro, phone, worktime,
                        type, is_new, longtitude, latitude));
            }

        } catch (JSONException | NullPointerException e) {
            e.printStackTrace();
            status.loadingError();
        }
//        сообщаем о том, что поток завершил свою работу
        status.loadingFinish(data);

        this.cancel(true);
    }
}

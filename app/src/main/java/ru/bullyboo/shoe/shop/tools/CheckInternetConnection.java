package ru.bullyboo.shoe.shop.tools;

import android.content.Context;
import android.net.ConnectivityManager;

/**
 * класс-помощник, проверяющий наличие интернета
 */
public class CheckInternetConnection {

    private Context context;

    public CheckInternetConnection(Context context){
        this.context = context;
    }

    public boolean isOnline() {
        String cs = context.CONNECTIVITY_SERVICE;
        ConnectivityManager cm = (ConnectivityManager)
                context.getSystemService(cs);
        if (cm.getActiveNetworkInfo() == null) {
            return false;
        } else {
            return true;
        }
    }
}

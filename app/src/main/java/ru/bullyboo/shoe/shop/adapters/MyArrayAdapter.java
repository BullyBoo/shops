package ru.bullyboo.shoe.shop.adapters;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import ru.bullyboo.shoe.shop.R;
import ru.bullyboo.shoe.shop.data.ReceivedData;

/**
 * Created by BullyBoo on 14.07.2016.
 */
public class MyArrayAdapter extends BaseAdapter {

    List<ReceivedData> data;

    Context context;

    public MyArrayAdapter(Context context, List<ReceivedData> data) {
        this.data = data;
        this.context = context;
    }

    public void setData(List<ReceivedData> data){
        this.data = data;
        notifyDataSetChanged();
    }

    public List<ReceivedData> getData(){
        return this.data;
    }

    @Override
    public int getCount() {
        return data.size();
    }

    @Override
    public Object getItem(int position) {
        return data.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        LayoutInflater inflater = LayoutInflater.from(context);

        if(convertView == null) convertView = inflater.inflate(R.layout.list_item, parent, false);

        ReceivedData receivedData = data.get(position);

        TextView name = (TextView) convertView.findViewById(R.id.name);
        TextView phone = (TextView) convertView.findViewById(R.id.phone);
        TextView town = (TextView) convertView.findViewById(R.id.town);
        TextView address = (TextView) convertView.findViewById(R.id.address);

        name.setText(receivedData.getName());
        phone.setText(receivedData.getPhone());
        town.setText(receivedData.getTown());
        address.setText(receivedData.getAddress() +
                "\nДистанция до магазина: " +
                (int) receivedData.getDistance() +
                " метров");

        return convertView;
    }

    public void sortAsc(List<ReceivedData> data){
        Collections.sort(data, new Comparator<ReceivedData>() {
            @Override
            public int compare(ReceivedData o1, ReceivedData o2) {
                return Double.compare(o1.getDistance(), o2.getDistance());
            }
        });
        notifyDataSetChanged();
    }

    public void sortDesc(List<ReceivedData> data){
        Collections.sort(data, new Comparator<ReceivedData>() {
            @Override
            public int compare(ReceivedData o1, ReceivedData o2) {
                int result = Double.compare(o1.getDistance(), o2.getDistance());

                if(result == 1){
                    return -1;
                }else if(result == -1){
                    return 1;
                }
                return 0;
            }
        });
        notifyDataSetChanged();
    }
}

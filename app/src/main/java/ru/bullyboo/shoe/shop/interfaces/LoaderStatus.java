package ru.bullyboo.shoe.shop.interfaces;

import android.support.annotation.UiThread;

import java.util.List;

import ru.bullyboo.shoe.shop.data.ReceivedData;

/**
 * Created by BullyBoo on 03.11.2016.
 */

public interface LoaderStatus {

    void noInternetConnection();

    @UiThread
    void loadingFinish(List<ReceivedData> data);

    @UiThread
    void loadingError();
}

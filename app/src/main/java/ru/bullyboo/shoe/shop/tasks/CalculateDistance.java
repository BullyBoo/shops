package ru.bullyboo.shoe.shop.tasks;

import android.location.Location;
import android.location.LocationManager;
import android.os.AsyncTask;
import java.util.List;

import ru.bullyboo.shoe.shop.data.ReceivedData;
import ru.bullyboo.shoe.shop.interfaces.CalculateDistanceStatus;
import ru.bullyboo.shoe.shop.tools.DistanceHelper;

/**
 * В этом потоке вычисляется расстояние в метрах от центра москвы до каждого магазина
 *
 */
public class CalculateDistance extends AsyncTask<Void, Void, Void> {

    private Location centerOfMoscow;

    private List<ReceivedData> list;

    private CalculateDistanceStatus state;

    private static final double CENTER_LATITUDE = 55.753928;
    private static final double CENTER_LONGTITUDE = 37.620510;

    public CalculateDistance(List<ReceivedData> list, CalculateDistanceStatus state){
        this.list = list;
        this.state = state;

        centerOfMoscow = new Location(LocationManager.GPS_PROVIDER);
        centerOfMoscow.setLatitude(CENTER_LATITUDE);
        centerOfMoscow.setLongitude(CENTER_LONGTITUDE);
    }

    @Override
    protected Void doInBackground(Void... params) {
//        считаем расстояние до магазина в метрах от центра города (Москва)
        for(ReceivedData d : list){

            double dist = DistanceHelper.distance(centerOfMoscow.getLatitude(),
                    d.getLatitude(),
                    centerOfMoscow.getLongitude(),
                    d.getLongtitude());

//            записываем полученную дистанцию
            d.setDistance(dist);
        }
        return null;
    }

    @Override
    protected void onPostExecute(Void s) {
        super.onPostExecute(s);
        state.finishCount(list);
    }
}

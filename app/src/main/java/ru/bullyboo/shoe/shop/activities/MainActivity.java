package ru.bullyboo.shoe.shop.activities;

import android.os.AsyncTask;
import android.support.annotation.UiThread;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.ListView;
import android.widget.ProgressBar;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import ru.bullyboo.shoe.shop.interfaces.LoaderStatus;
import ru.bullyboo.shoe.shop.interfaces.CalculateDistanceStatus;
import ru.bullyboo.shoe.shop.tasks.DataDownload;
import ru.bullyboo.shoe.shop.adapters.MyArrayAdapter;
import ru.bullyboo.shoe.shop.R;
import ru.bullyboo.shoe.shop.data.ReceivedData;
import ru.bullyboo.shoe.shop.tasks.CalculateDistance;


public class MainActivity extends AppCompatActivity implements LoaderStatus,
        CalculateDistanceStatus {

    private ListView list;
    private ProgressBar progressBar;
    private TextView text;

    private DataDownload download;
    private CalculateDistance distance;

    private MyArrayAdapter adapter;

    private boolean loadingSuccessfullFinish = false;
    private boolean isSorted = false;
    private boolean ascending = false;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);

        text = (TextView) findViewById(R.id.textView);
        text.setVisibility(View.INVISIBLE);

        progressBar = (ProgressBar) findViewById(R.id.progressBar);
//        запускаем поток, который выгружает данные из json
        startLoading();
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();

        if (id == R.id.sort) {
            sorting();
        }

        return super.onOptionsItemSelected(item);
    }

    private void sorting(){
        if(loadingSuccessfullFinish)
            if(isSorted){
                if(ascending){
                    ascending = false;
                    adapter.sortDesc(adapter.getData());
                }else{
                    ascending = true;
                    adapter.sortAsc(adapter.getData());
                }
            }else{
                startSorting();
            }
        else
            Toast.makeText(this, "Дождитесь окончания загрузки", Toast.LENGTH_LONG).show();
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        stopTask(download);
        stopTask(distance);
    }

    @Override
    @UiThread
    public void noInternetConnection() {
        text.setVisibility(View.INVISIBLE);

        stopTask(download);
    }

    @Override
    public void loadingFinish(List<ReceivedData> data) {
//        отображаем результаты в listView
        loadingSuccessfullFinish = true;

        adapter = new MyArrayAdapter(MainActivity.this, data);

        list = (ListView) findViewById(R.id.listView);
        list.setAdapter(adapter);

        stopTask(download);
    }

    @Override
    public void loadingError() {
        Toast.makeText(this,
                "Произошла ошибка\nСейчас попробуем еще раз...",
                Toast.LENGTH_LONG).show();

        stopTask(download);
        startLoading();
    }

    @Override
    public void finishCount(List<ReceivedData> data) {
//        сортируем список
        adapter.setData(data);
        adapter.sortAsc(data);
//        обновляем данные

        stopTask(distance);
//        выставляем флаги
        isSorted = true;
        ascending = true;
    }

    private void startLoading(){
        download = new DataDownload(this, this);
        download.execute();
    }

    private void startSorting(){
        distance = new CalculateDistance(adapter.getData(), this);
        distance.execute();

        progressBar.setVisibility(View.VISIBLE);
    }

    private void stopTask(AsyncTask task){
        if(task != null)
            task.cancel(true);

        progressBar.setVisibility(View.INVISIBLE);
    }
}

package ru.bullyboo.shoe.shop.interfaces;

import java.util.List;

import ru.bullyboo.shoe.shop.data.ReceivedData;

/**
 * Created by BullyBoo on 14.07.2016.
 */
public interface CalculateDistanceStatus{

    void finishCount(List<ReceivedData> data);

}
